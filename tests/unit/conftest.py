import pytest
from dict_tools import data
import google.auth.credentials as google_auth


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    return hub


@pytest.fixture
def ctx(session_backend_lib):
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(credentials=google_auth.AnonymousCredentials())
    )
