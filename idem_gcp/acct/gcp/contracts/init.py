from google.auth.credentials import Credentials
from typing import Any, Dict


def sig_gather(hub) -> Dict[str, Any]:
    ...


def post_gather(hub, ctx) -> Dict[str, Any]:
    """
    Validate the return from a "gather" function
    """
    for profile, profile_ctx in ctx.ret.items():
        error_msg = f"{ctx.ref} profile formatted incorrectly: {profile}"
        assert "credentials" in profile_ctx, error_msg
        assert isinstance(profile_ctx["credentials"], Credentials), error_msg

    return ctx.ret
