from typing import Any, Dict


def pre(hub, ctx):
    kwargs = ctx.get_arguments()
    if "ctx" in kwargs:
        assert (
            "credentials" in kwargs["ctx"].acct
        ), f"No credentials found for profile: {hub.OPT.acct.get('acct_profile')}"


def _format_obj(obj: object) -> Dict[str, Any]:
    return {str(k): v for k, v in obj.__dict__.items() if not k.startswith("_")}


async def post_get(hub, ctx):
    return ctx.ret
    return _format_obj(ctx.ret)


async def post_list(hub, ctx):
    return ctx.ret
    return [_format_obj(x) for x in ctx.ret]
