def _wrap_iam_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.iam.projects.init.service(ctx)
        with service.serviceAccounts() as service_accounts:
            func = getattr(service_accounts, target)
            request = func(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in (
        "disable",
        "patch",
        "create",
        "update",
        "delete",
        "list",
        "enable",
        "signBlob",
        "setIamPolicy",
        "getIamPolicy",
        "undelete",
        "get",
        "signJwt",
        "testIamPermissions",
        "keys",
        "list_next",
    ):
        out[func] = _wrap_iam_client(hub, func)

    return out
