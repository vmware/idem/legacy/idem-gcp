def _wrap_iam_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.iam.init.service(ctx)
        with service.iamPolicies() as policies:
            request = getattr(policies, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in ("lintPolicy", "queryAuditableServices"):
        out[func] = _wrap_iam_client(hub, func)

    return out
