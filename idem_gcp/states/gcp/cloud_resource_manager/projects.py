# https://cloud.google.com/resource-manager/docs/creating-managing-projects#python


def _wrap_crm_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.crm.init.service(ctx)
        with service.projects() as projects:
            request = getattr(projects, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in (
        "get",
        "list",
        "create",
        "update",
        "delete",
        "undelete",
        "setIamPolicy",
        "clearOrgPolicy",
        "listOrgPolicies",
        "getOrgPolicy",
        "getIamPolicy",
        "getEffectiveOrgPolicy",
        "listAvailableOrgPolicyConstraints",
        "getAncestry",
        "testIamPermissions",
        "setOrgPolicy",
        "list_next",
        "listOrgPolicies_next",
        "listAvailableOrgPolicyConstraints_next",
    ):
        out[func] = _wrap_crm_client(hub, func)
    return out
